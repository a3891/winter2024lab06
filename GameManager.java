public class GameManager {
    private Deck drawPile;
    private Card centerCard;
    private Card playerCard;

    public GameManager() {
        drawPile = new Deck();
        drawPile.shuffle();
        centerCard = drawPile.drawTopCard();
        playerCard = drawPile.drawTopCard();
    }

    public String toString() {
        return "-------------------------------------------------------------\n" +
                "Center card: " + centerCard + "\n" +
                "Player card: " + playerCard + "\n" +
                "-------------------------------------------------------------";
    }

    public void dealCards() {
        drawPile.shuffle();
        centerCard = drawPile.drawTopCard();
        playerCard = drawPile.drawTopCard();
    }

    public int getNumberOfCards() {
        return drawPile.length();
    }

    public int calculatePoints() {
        if (centerCard.getValue().equals(playerCard.getValue())) {
            return 4;
        } 
        else if (centerCard.getSuit().equals(playerCard.getSuit())) {
            return 2;
        } 
        else {
            return -1;
        }
    }
}