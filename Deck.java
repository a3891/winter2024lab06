import java.util.Random;

public class Deck {
    private Card[] cards;
    private int numberOfCards;
    private Random rng;

    public Deck() {
        rng = new Random();
        numberOfCards = 52;
        cards = new Card[numberOfCards];

        String[] suits = {"Hearts", "Clubs", "Diamonds", "Spades"};
        String[] values = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King", "Ace"};

        //looping to fill the array with cards
        int index = 0;
        for (String suit : suits) {
            for (String value : values) {
                cards[index] = new Card(suit, value);
                index++;
            }
        }
    }

    public int length() {
        return numberOfCards;
    }

    public Card drawTopCard() {
        numberOfCards--;
        return cards[numberOfCards];
    }

    public String toString() {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < numberOfCards; i++) {
            result.append(cards[i].toString()).append("\n");
        }
        return result.toString();
    }

    // Method to shuffle the cards in the deck
    public void shuffle() {
        for (int i = 0; i < numberOfCards; i++) {
            int randomIndex = i + rng.nextInt(numberOfCards - i);
            // Swap the current card with the randomly chosen card
            Card temp = cards[i];
            cards[i] = cards[randomIndex];
            cards[randomIndex] = temp;
        }
    }
}