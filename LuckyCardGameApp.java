import java.util.Scanner;

public class LuckyCardGameApp {
    public static void main (String[] args) {
        
        GameManager manager = new GameManager();
        int totalPoints = 0;
        int rounds = 0; //addition by me to keep track of the number of rounds/tries
        System.out.println("Welcome to the Lucky Card Game! Good luck.");

        while (manager.getNumberOfCards() > 1 && totalPoints < 5) {
            System.out.println(manager);
            int pointsReceived = manager.calculatePoints(); //Upgraded the code to also see the amount of points recieved 
            totalPoints += pointsReceived;                  //after each round, to not do mental calculations
            System.out.println("Points received: " + pointsReceived);
            System.out.println("Total points: " + totalPoints);
            manager.dealCards();
            rounds++;
        }
        
        System.out.println("\n-------------------------------------------------------------");
        System.out.println("Game ended!\nFinal score: " + totalPoints);
        if (totalPoints < 5) {
            System.out.println("You lost!");
        } 
        else {
            System.out.println("You won! Amount of tries: " + rounds);
        }
    }
}